<?php wp_footer(); ?>
</div>

<script>
    loadjs("<?php echo esc_url( get_template_directory_uri() ); ?>/javascript/min/main.min.js", "mainJS");
</script>

<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/min/jquery.js"></script>

</body>
</html>